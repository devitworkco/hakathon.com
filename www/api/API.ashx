﻿<%@ WebHandler Language="C#" Class="API" %>
// http://stackoverflow.com/questions/14097785/how-can-i-return-a-404-error-from-an-asp-net-handler

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Reflection;
using System.Web;

public class API : IHttpHandler
{
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        HttpRequest request = context.Request;
        HttpResponse response = context.Response;


        string cls = request["cls"].Trim();
        string fn = request["mthd"].Trim();

        var settings = new JsonSerializerSettings();
        settings.ContractResolver = new MyLowercaseContractResolverCls();

        object obj = GetInstance(cls);
        Type t = obj.GetType();
        MethodInfo method = t.GetMethod(fn);
        Object[] prms = new Object[method.GetParameters().Length];
        for (int i = 0; i < method.GetParameters().Length; i++)
        {
            ParameterInfo pi = method.GetParameters()[i];

            if (string.IsNullOrEmpty(context.Request[pi.Name])) { continue; }

            dynamic p_val;
            try
            {
                p_val = JsonConvert.DeserializeObject(context.Request[pi.Name], pi.ParameterType);
            }
            catch (Exception ex)
            {
                p_val = context.Request[pi.Name];
            }

            prms[i] = p_val;
            try
            { 
                prms[i] = Convert.ChangeType(prms[i], pi.ParameterType);
            }
            catch (Exception e)
            { 
            }
        }

        Object retVal = null;
        retVal = method.Invoke(obj, prms);
        if (retVal == null) retVal = "1";
        if (retVal.GetType() != "".GetType())
        {
            retVal = JsonConvert.SerializeObject(retVal, settings);
        }

        FlushResponse(retVal.ToString());

    }


    public static void FlushResponse(string ret, string ContentType = "text/html;  encoding=utf-8;")
    {
        HttpContext.Current.Response.Clear();
        HttpContext.Current.Response.ContentType = ContentType;
        byte[] b = HttpContext.Current.Response.ContentEncoding.GetBytes(ret);
        HttpContext.Current.Response.AddHeader("Content-Length", b.Length.ToString());
        HttpContext.Current.Response.Write(ret.ToString());
        HttpContext.Current.Response.Flush();
        HttpContext.Current.Response.End();
        HttpContext.Current.Response.Close();
    }
    static object GetInstance(string strFullyQualifiedName)
    {

        //http://stackoverflow.com/questions/223952/create-an-instance-of-a-class-from-a-string
        Type type = Type.GetType(strFullyQualifiedName);
        if (type != null)
            return Activator.CreateInstance(type);
        foreach (var asm in AppDomain.CurrentDomain.GetAssemblies())
        {
            type = asm.GetType(strFullyQualifiedName);
            if (type != null)
                return Activator.CreateInstance(type);
            type = asm.GetType(asm.GetName().Name + "." + strFullyQualifiedName);
            if (type != null)
                return Activator.CreateInstance(type);

        }
        return null;
    }


}
public class MyLowercaseContractResolverCls : DefaultContractResolver
{
    protected override string ResolvePropertyName(string propertyName)
    {
        return propertyName.ToLower();
    }
}