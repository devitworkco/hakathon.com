﻿using Newtonsoft.Json.Linq;
using Nustache.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for code
/// </summary>
public class code
{
    public static dynamic order_new(dynamic   obj) {
         
       dynamic   opts = obj;
        var od_parm = new  
        {
            name = DApp.clsHelper.CookieGet("uid"),
            order_number = DateTime.Now.ToString("yyMMddmms"),
            condition = "new"
        };
        dynamic  ord = DApp.dCall.init().Set("order").Uid("new").Data(od_parm).Go();

        foreach (dynamic  item in opts.products)
        { 
            DApp.dCall.init().Set("order_details").Uid("new").Data(new {
                orders_number = Convert.ToString(ord.uid),
                price = item.price,
                prods= item.uid,
                quant = (item.count == null ? 1: item.count)
            }).Go();

        }


        string temp = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("~/Riot/html_templates/order_new.html"));

        string  html = Render.StringToString(temp,new {
            order = od_parm,
            products =   opts.products,
            user = new {
                name  = DApp.clsHelper.CookieGet("name"),
                phone = DApp.clsHelper.CookieGet("phone"),
                address = DApp.clsHelper.CookieGet("address")
            }
        });


       dhelper.sendemail("www@almarwanistore.com", "ma.fawzy.it@gmail.com,info@almarwanistore.com", "طلب جديد رقم " + Convert.ToString(od_parm.order_number), html);


        return ord;
    }
}