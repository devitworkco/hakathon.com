﻿using Newtonsoft.Json; 
using Newtonsoft.Json.Serialization;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Xml;
using HttpCookie = System.Web.HttpCookie;
using System.Linq;
using Newtonsoft.Json.Linq;


namespace DApp
{
    public class dCall
    {
        private dCall _obj;
        private string _method = "";
        private string _doctype = "";
        private JObject _where = new JObject();
        private JObject _sort = new JObject();
        private object _data;
        private string _uid = "";
        private int _length = 50;

        public dCall()
        {
            _obj = this;
        }

        public dCall(string doctype, string method)
        {
            _obj = this;
            _obj._method = method;
            _obj._doctype = doctype;
        }

        public static dCall init()
        {
            return new dCall();
        }

        public dynamic Exec(string cls, string mthd, RestRequest rstRequest)
        {
            _obj = this;
            RestClient rstClient = new RestClient(clsHelper.ConfigGet("api_url"));
            rstRequest.Method = Method.POST;
            rstRequest.Resource = "/api/{cls}/{mthd}/";
            rstRequest.AddUrlSegment("cls", cls);
            rstRequest.AddUrlSegment("mthd", mthd);

            string lang = clsHelper.RequestValueFix("lang", clsHelper.ConfigGet("lang"));
            rstRequest.AddParameter("lang", lang, ParameterType.GetOrPost);
            rstRequest.AddParameter("loginuid", GetLoginUid(), ParameterType.GetOrPost);

            IRestResponse rstResponse = rstClient.Execute(rstRequest);

            string log = clsHelper.ConfigGet("api_log").ToString();
            if (log == "true")
            {
                var dir = System.IO.Directory.CreateDirectory(HttpContext.Current.Server.MapPath("~/_debug/" + cls + "/" + DateTime.Now.ToString("yyyy-MM-dd") + "/" + mthd + "/" + Guid.NewGuid().ToString() + "/"));
                File.WriteAllText(
                    dir.FullName + "/request.json",
                    clsHelper.SerializeObject(
                        new
                        {
                            data = rstRequest.Parameters,
                            url = rstRequest.Resource
                        }
                    )
                );
                File.WriteAllText(dir.FullName + "/response.html", rstResponse.Content);
            }

            return clsHelper.DeserializeObject(rstResponse.Content);
        }

        public IEnumerable<dynamic> Go()
        {
            _obj = this;

            dynamic parm = new System.Dynamic.ExpandoObject();
            parm.length = _obj._length;
            if (!string.IsNullOrEmpty(_obj._uid)) parm.uid = _obj._uid;
            if (_obj._where.HasValues) parm.where = _obj._where;
            if (_obj._sort.HasValues) parm.sort = _obj._sort;
            if (_obj._data != null) parm.data = _obj._data;

            RestRequest rstRequest = new RestRequest();
            rstRequest.AddParameter("obj", clsHelper.SerializeObject(parm), ParameterType.GetOrPost);

            dynamic data = Exec(_obj._method, _obj._doctype, rstRequest);
             

            return data.ToObject<IEnumerable<dynamic>>();
        }

        public dynamic Register(dynamic d)
        {
            _obj = this;

            dynamic parm = new System.Dynamic.ExpandoObject();

            parm.uid = "new";
            parm.data = d;

            RestRequest rstRequest = new RestRequest();
            rstRequest.AddParameter("obj", clsHelper.SerializeObject(parm), ParameterType.GetOrPost);
            Exec("user", "register", rstRequest);
            return Login(d.username, d.password);
        }

        public dynamic Login(string username, string password)
        {
            _obj = this;

            dynamic parm = new System.Dynamic.ExpandoObject();

            parm.agencyid = clsHelper.ConfigGet("api_agency_id");
            parm.password = password;
            parm.username = username;

            RestRequest rstRequest = new RestRequest();
            rstRequest.AddParameter("obj", clsHelper.SerializeObject(parm), ParameterType.GetOrPost);

            return Exec("user", "login", rstRequest);
        }

        public dCall Data(object data)
        {
            _obj = this;
            _obj._data = (data == null ? new object() : data);
            return _obj;
        }

        public dCall Sort(string key, JToken value,string fn)
        {
            _obj = this;
            _obj._sort.Add(key, JObject.FromObject(new
            {
                dir = value,
                fn = fn
            }));
            return _obj;
        }
        public dCall Sort(string key, JToken value)
        {
            _obj = this;
            _obj._sort.Add(key, JObject.FromObject(new {
                dir = value
            }));
            return _obj;
        }

        public dCall Where(string key, JToken value)
        {
            _obj = this;
            _obj._where.Add(key, value);
            return _obj;
        }

        public dCall Uid(string uid)
        {
            _obj = this;
            _obj._uid = uid;
            return _obj;
        }

        public dCall Length(int length)
        {
            _obj = this;
            _obj._length = length;
            return _obj;
        }

        public dCall Get(string doctype)
        {
            _obj = this;
            _obj._method = "get";
            _obj._doctype = doctype;
            return _obj;
        }

        public dCall Set(string doctype)
        {
            _obj = this;
            _obj._method = "set";
            _obj._doctype = doctype;
            return _obj;
        }

        private string GetLoginUid()
        {
            string Login_Uid = clsHelper.CookieGet("Login_Uid");

            if (string.IsNullOrEmpty(Login_Uid))
            {
                RestClient rstClient = new RestClient(clsHelper.ConfigGet("api_url"));
                RestRequest rstRequest = new RestRequest("/api/user/login/");

                rstRequest.AddParameter("obj", clsHelper.SerializeObject(new
                {
                    agencyid = clsHelper.ConfigGet("api_agency_id"),
                    username = clsHelper.ConfigGet("api_user"),
                    password = clsHelper.ConfigGet("api_password")
                }), ParameterType.GetOrPost);

                IRestResponse rstResponse = rstClient.Execute(rstRequest);
                Login_Uid = clsHelper.DeserializeObject(rstResponse.Content).loginuid;
                clsHelper.CookieSet("Login_Uid", Login_Uid);
            }

            return Login_Uid;
        }
    }

    public static class clsHelper
    {
        #region "Encrypt & Decrypt"

        public static string AES_Encrypt(string input)
        {
            string pass = "ITw0rE7|";
            System.Security.Cryptography.RijndaelManaged AES = new System.Security.Cryptography.RijndaelManaged();
            System.Security.Cryptography.MD5CryptoServiceProvider Hash_AES = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string encrypted = "";
            byte[] hash = new byte[32];
            byte[] temp = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass));
            Array.Copy(temp, 0, hash, 0, 16);
            Array.Copy(temp, 0, hash, 15, 16);
            AES.Key = hash;
            AES.Mode = System.Security.Cryptography.CipherMode.ECB;
            System.Security.Cryptography.ICryptoTransform DESEncrypter = AES.CreateEncryptor();
            byte[] Buffer = System.Text.ASCIIEncoding.ASCII.GetBytes(input);
            encrypted = Convert.ToBase64String(DESEncrypter.TransformFinalBlock(Buffer, 0, Buffer.Length));
            return encrypted;
        }

        public static string AES_Decrypt(string input)
        {
            string pass = "ITw0r7|";
            System.Security.Cryptography.RijndaelManaged AES = new System.Security.Cryptography.RijndaelManaged();
            System.Security.Cryptography.MD5CryptoServiceProvider Hash_AES = new System.Security.Cryptography.MD5CryptoServiceProvider();
            string decrypted = "";
            byte[] hash = new byte[32];
            byte[] temp = Hash_AES.ComputeHash(System.Text.ASCIIEncoding.ASCII.GetBytes(pass));
            Array.Copy(temp, 0, hash, 0, 16);
            Array.Copy(temp, 0, hash, 15, 16);
            AES.Key = hash;
            AES.Mode = System.Security.Cryptography.CipherMode.ECB;
            System.Security.Cryptography.ICryptoTransform DESDecrypter = AES.CreateDecryptor();

            byte[] Buffer = Convert.FromBase64String(input);
            decrypted = System.Text.ASCIIEncoding.ASCII.GetString(DESDecrypter.TransformFinalBlock(Buffer, 0, Buffer.Length));
            return decrypted;
        }

        #endregion "Encrypt & Decrypt"

        #region "Json Work Area"

        public static dynamic DeserializeObject(dynamic obj)
        {
            dynamic settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolverCls();
            return JsonConvert.DeserializeObject(obj, settings);
        }

        public static string SerializeObject(dynamic obj)
        {
            dynamic settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolverCls();
            return JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.Indented, settings);
        }

        public static string SerializeXmlNode(dynamic obj)
        {
            dynamic settings = new JsonSerializerSettings();
            settings.ContractResolver = new LowercaseContractResolverCls();
            return JsonConvert.SerializeXmlNode(obj, Newtonsoft.Json.Formatting.Indented, false);
        }

        public static string SerializeXmlNode(string obj)
        {
            XmlDocument doc_old = new XmlDocument();
            doc_old.LoadXml(obj);
            return SerializeXmlNode(doc_old);
        }

        #endregion "Json Work Area"

        #region "XML Area"

        public static string XmlFull(DataTable dt)
        {
            MemoryStream str = new MemoryStream();
            dt.WriteXml(str, true);
            str.Seek(0, SeekOrigin.Begin);
            StreamReader sr = new StreamReader(str);
            string xmlstr = null;
            xmlstr = sr.ReadToEnd();
            return (xmlstr);
        }

        #endregion "XML Area"

        #region "Cookie"

        public static void CookieSet(string CookieName, string value)
        {

            CookieName = CookieName + "_"+ HttpContext.Current.Request.Url.Port;

            HttpCookie myCookie = new HttpCookie(CookieName);
            //myCookie.Value = AES_Encrypt(value);
            myCookie.Value = HttpUtility.UrlEncode(value);
            myCookie.Expires = DateTime.Now.AddDays(2);
            HttpContext.Current.Response.Cookies.Add(myCookie);
        }

        public static void CookieDelete(string CookieName)
        {
            CookieName = CookieName + "_" + HttpContext.Current.Request.Url.Port;
            HttpCookie myCookie = default(HttpCookie);
            myCookie = new HttpCookie(CookieName);
            myCookie.Expires = DateTime.Now.AddDays(-1);
            myCookie.Value = "";
            HttpContext.Current.Response.Cookies.Add(myCookie);
            HttpContext.Current.Request.Cookies.Remove(CookieName);
        }

        public static string CookieGet(string CookieName)
        {
            try
            {
                CookieName = CookieName + "_" + HttpContext.Current.Request.Url.Port;
                //return AES_Decrypt(HttpContext.Current.Request.Cookies[CookieName].Value.ToString());
                return HttpUtility.UrlDecode(HttpContext.Current.Request.Cookies[CookieName].Value.ToString());
            }
            catch (Exception ex)
            {
                // HttpContext.Current.Response.Redirect("/login.aspx");
                return null;
            }
        }

        #endregion "Cookie"

        public static bool IsPropertyExist(object obj, string name)
        {
            try
            {
                if (obj.GetType() == typeof(JObject))
                {
                    return JObject.FromObject(obj)[name] != null;
                }
                return obj.GetType().GetProperty(name.Trim()) != null;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static void FlushResponse(string ret, string ContentType = "text/html;  encoding=utf-8;")
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = ContentType;
            byte[] b = HttpContext.Current.Response.ContentEncoding.GetBytes(ret);
            HttpContext.Current.Response.AddHeader("Content-Length", b.Length.ToString());
            HttpContext.Current.Response.Write(ret.ToString());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
            HttpContext.Current.Response.Close();
        }

        public static DataTable DataTableFilterByDataView(DataTable dt, string str)
        {
            DataView dv = dt.DefaultView;
            dv.RowFilter = str;
            return dv.ToTable();
        }

        public static string HtmlStrip(object html)
        {
            return Regex.Replace(Convert.ToString(html), "<.*?>", "");
        }

        public static string SubString(object val, int KeysCount)
        {
            string str = HtmlStrip(val);
            string[] arry = str.Split(' ');
            if (arry.Length > KeysCount)
            {
                str = "";
                for (int i = 0; i <= KeysCount; i++)
                {
                    str = str + arry[i] + " ";
                }
                str = str + " ...";
            }
            return str;
        }

        public static string ConfigGet(string key)
        {
            return ConfigurationManager.AppSettings[key].ToString();
        }

        public static string RequestValueFix(string key, string def_val)
        {
            string val = HttpContext.Current.Request[key];
            if (string.IsNullOrEmpty(val))
                return def_val;
            else if (val.ToString().Trim().Length == 0 & val.GetType() == typeof(string))
                return def_val;
            else
                return val;
        }

        private static string YoutubeLinkRegex = "(?:.+?)?(?:\\/v\\/|watch\\/|\\?v=|\\&v=|youtu\\.be\\/|\\/v=|^youtu\\.be\\/)([a-zA-Z0-9_-]{11})+";
        private static Regex regexExtractId = new Regex(YoutubeLinkRegex, RegexOptions.Compiled);
        private static string[] validAuthorities = { "youtube.com", "www.youtube.com", "youtu.be", "www.youtu.be" };

        public static string ExtractVideoIdFromUri(string url)
        {
            try
            {
                Uri uri = new Uri(url);
                string authority = new UriBuilder(uri).Uri.Authority.ToLower();

                //check if the url is a youtube url
                if (validAuthorities.Contains(authority))
                {
                    //and extract the id
                    var regRes = regexExtractId.Match(uri.ToString());
                    if (regRes.Success)
                    {
                        return regRes.Groups[1].Value;
                    }
                }
            }
            catch { }


            return null;
        }

    }

    public class LowercaseContractResolverCls : DefaultContractResolver
    {
        protected override string ResolvePropertyName(string propertyName)
        {
            return propertyName.ToLower();
        }
    }


}

public class dhelper
{
    public static JArray exec(object obj)
    {
        if (obj == null)
        {
            obj = new { };
        }
        string cls = HttpContext.Current.Request["doctype"].ToString();
        string mthd = HttpContext.Current.Request["fn"].ToString();
        var dcall = new DApp.dCall();

        RestRequest rstRequest = new RestRequest();
        rstRequest.AddParameter("obj", DApp.clsHelper.SerializeObject(obj), ParameterType.GetOrPost);

        return dcall.Exec(cls, mthd, rstRequest); ;
    }

    public static bool islogin()
    {
        return !string.IsNullOrEmpty(DApp.clsHelper.CookieGet("IsLogin"));
    }

    public static string sendemail(string from, string to, string subject, string message)
    {
        var dcall = new DApp.dCall();
        dynamic parm = new System.Dynamic.ExpandoObject();
        parm.from = from;
        parm.to = to;
        parm.subject = subject;
        parm.message = message;

        RestRequest rstRequest = new RestRequest();
        rstRequest.AddParameter("obj", DApp.clsHelper.SerializeObject(parm), ParameterType.GetOrPost);

        dcall.Exec("emailapi", "send", rstRequest);
        return "1";
    }
}